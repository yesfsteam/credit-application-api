﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditApplication.Api.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.Infrastructure.Common.Models;
using CreditApplicationListFilterModel = Yes.CreditApplication.Api.Contracts.Admin.CreditApplicationListFilterModel;
using CreditApplicationListItemModel = Yes.CreditApplication.Api.Contracts.Admin.CreditApplicationListItemModel;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.Admin.CreditApplicationRequest;

namespace CreditApplication.Api.Controllers
{
    [Route("api/v1/credit-applications")]
    public class AdminCreditApplicationsController : ControllerBase
    {
        private readonly ICreditApplicationManager creditApplicationManager;

        public AdminCreditApplicationsController(ICreditApplicationManager creditApplicationManager)
        {
            this.creditApplicationManager = creditApplicationManager;
        }
        
        
        /// <summary>
        /// Возвращает список заявок на кредит по заданному фильтру
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(Models.Admin.PagedResult<CreditApplicationListItemModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCreditApplications([FromQuery]CreditApplicationListFilterModel model)
        {
            var response = await creditApplicationManager.GetCreditApplications(model);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Возвращает список заявок на кредит по заданному фильтру, для выгрузки в файл
        /// </summary>
        [HttpGet("export")]
        [ProducesResponseType(typeof(List<CreditApplicationListItemExportModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCreditApplicationsForExport([FromQuery]CreditApplicationListFilterModel model)
        {
            var response = await creditApplicationManager.GetCreditApplicationsForExport(model);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Возвращает информацию по заявке для просмотра
        /// </summary>
        [HttpGet("{creditApplicationId}")]
        [ProducesResponseType(typeof(CreditApplicationViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCreditApplicationViewModel(Guid creditApplicationId)
        {
            var response = await creditApplicationManager.GetCreditApplicationViewModel(creditApplicationId);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Возвращает информацию по заявке для редактирования
        /// </summary>
        [HttpGet("{creditApplicationId}/edit")]
        [ProducesResponseType(typeof(CreditApplicationEditViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCreditApplicationEditViewModel(Guid creditApplicationId)
        {
            var response = await creditApplicationManager.GetCreditApplicationEditViewModel(creditApplicationId);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Возвращает список решений по заявке
        /// </summary>
        [HttpPost("{creditApplicationId}/decisions")]
        [ProducesResponseType(typeof(CreditApplicationResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCreditApplicationDecisions([FromRoute]Guid creditApplicationId)
        {
            var response = await creditApplicationManager.GetCreditApplicationDecisions(creditApplicationId);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Отправляет заново запрос на кредит
        /// </summary>
        [HttpPost("{creditApplicationId}/retry")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ResendCreditApplicationRequest([FromRoute]Guid creditApplicationId, CreditApplicationRequest model)
        {
            var response = await creditApplicationManager.ResendCreditApplicationRequest(creditApplicationId, model.CreditOrganizationId);
            return MakeResponse(response);
        }

        /// <summary>
        /// Подтверждает заявку на кредит
        /// </summary>
        [HttpPost("{creditApplicationId}/confirmation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ConfirmCreditApplication([FromRoute]Guid creditApplicationId, CreditApplicationRequest model)
        {
            var response = await creditApplicationManager.ConfirmCreditApplication(creditApplicationId, model.CreditOrganizationId);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Обновляет статус заявки
        /// </summary>
        [HttpPut("{creditApplicationId}/status")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ChangeCreditApplicationStatus([FromRoute]Guid creditApplicationId, ChangeCreditApplicationStatusModel model)
        {
            var response = await creditApplicationManager.ChangeCreditApplicationStatus(creditApplicationId, model);
            return MakeResponse(response);
        }

        /// <summary>
        /// Обновляет данные заявки
        /// </summary>
        [HttpPut("{creditApplicationId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateCreditApplication([FromRoute]Guid creditApplicationId, UpdateCreditApplicationModel model)
        {
            var response = await creditApplicationManager.UpdateCreditApplication(creditApplicationId, model);
            return MakeResponse(response);
        }
    }
}
