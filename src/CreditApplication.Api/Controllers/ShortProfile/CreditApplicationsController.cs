﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Domain.Public.ShortProfile;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yes.CreditApplication.Api.Contracts.Public;
using Yes.CreditApplication.Api.Contracts.Public.ShortProfile;
using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Controllers.ShortProfile
{
    [Route("api/v3/credit-applications")]
    public class CreditApplicationsController : ControllerBase
    {
        private readonly ICreditApplicationManager creditApplicationManager;

        public CreditApplicationsController(ICreditApplicationManager creditApplicationManager)
        {
            this.creditApplicationManager = creditApplicationManager;
        }
        
        /// <summary>
        /// Создает заявку на кредит (Шаг 1)
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCreditApplication(CreateCreditApplicationModel model)
        {
            var response = await creditApplicationManager.CreateCreditApplication(model);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Отправляет новый код подтверждения (Шаг 2)
        /// </summary>
        [HttpPost("{creditApplicationId}/new-confirmation-code")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ResendConfirmationCode([FromRoute]Guid creditApplicationId)
        {
            var response = await creditApplicationManager.ResendConfirmationCode(creditApplicationId);
            return MakeResponse(response);
        }

        /// <summary>
        /// Подтверждает номер телефона клиента (Шаг 2)
        /// </summary>
        [HttpPost("{creditApplicationId}/confirmation-code")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ConfirmPhoneNumber([FromRoute]Guid creditApplicationId, CreditApplicationConfirmationCodeModel model)
        {
            var response = await creditApplicationManager.ConfirmPhoneNumber(creditApplicationId, model.ConfirmationCode);
            return MakeResponse(response);
        }
    }
}
