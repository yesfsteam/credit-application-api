﻿using Microsoft.AspNetCore.Mvc;
using Yes.Infrastructure.Http;

namespace CreditApplication.Api.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class ControllerBase: Controller
    {
        protected IActionResult MakeResponse<T>(Response<T> result)
        {
            if (!result.IsSuccessStatusCode)
            {
                return StatusCode((int)result.StatusCode, result.ErrorMessage);
            }

            return Ok(result.Content);
        }
        
        protected IActionResult MakeResponse<T, TBadRequestModel>(Response<T, TBadRequestModel> result)
        {
            if (!result.IsSuccessStatusCode)
            {
                return StatusCode((int)result.StatusCode, result.ErrorModel);
            }

            return Ok(result.Content);
        }
        
        protected IActionResult MakeResponse(Response result)
        {
            if (!result.IsSuccessStatusCode)
            {
                return StatusCode((int)result.StatusCode, result.ErrorMessage);
            }

            return Ok();
        }
    }
}