﻿using System.Threading.Tasks;
using CreditApplication.Api.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CreditApplication.Api.Controllers
{
    [Route("api/v1/maintenance")]
    public class MaintenanceController : ControllerBase
    {
        private readonly ICreditApplicationManager creditApplicationManager;

        public MaintenanceController(ICreditApplicationManager creditApplicationManager)
        {
            this.creditApplicationManager = creditApplicationManager;
        }
        
        /// <summary>
        /// Удаляет неподтвержденные заявки на кредит
        /// </summary>
        [HttpDelete("credit-applications")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteExpiredCreditApplications()
        {
            var response = await creditApplicationManager.DeleteExpiredCreditApplications();
            return MakeResponse(response);
        }    
    }
}
