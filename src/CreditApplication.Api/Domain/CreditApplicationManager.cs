﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditApplication.Api.Data;
using CreditApplication.Api.Extensions;
using CreditApplication.Api.Models.Commands;
using CreditApplication.Api.Models.Configuration;
using CreditApplication.Api.Models.Enums;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Public;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;
using Yes.Sms.Api.Contracts.Commands;

namespace CreditApplication.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<Response<PagedResult<CreditApplicationListItemModel>>> GetCreditApplications(CreditApplicationListFilterModel model);
        Task<Response<List<CreditApplicationListItemExportModel>>> GetCreditApplicationsForExport(CreditApplicationListFilterModel model);
        Task<Response<CreditApplicationViewModel>> GetCreditApplicationViewModel(Guid creditApplicationId);
        Task<Response<CreditApplicationEditViewModel>> GetCreditApplicationEditViewModel(Guid creditApplicationId);
        Task<Response<List<CreditApplicationDecisionModel>>> GetCreditApplicationDecisions(Guid creditApplicationId);
        Task<Response<EmptyModel, ErrorModel>> ConfirmCreditApplication(Guid creditApplicationId, Guid creditOrganizationId);
        Task<Response<EmptyModel, ErrorModel>> ResendCreditApplicationRequest(Guid creditApplicationId, Guid creditOrganizationId);
        Task<Response> ChangeCreditApplicationStatus(Guid creditApplicationId, ChangeCreditApplicationStatusModel model);
        Task<Response> UpdateCreditApplication(Guid creditApplicationId, UpdateCreditApplicationModel model);
        
        Task<Response> DeleteExpiredCreditApplications();
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly IEventPublisher eventPublisher;
        private readonly ICommandSender commandSender;
        private readonly IConfirmationCodeGenerator generator;
        private readonly ICreditOrganizationRepository creditOrganizationRepository;
        private readonly ICreditApplicationDecisionRepository decisionRepository;
        private readonly ICreditApplicationChangelogRepository changelogRepository;
        private readonly ICreditOrganizationClient client;
        private readonly ApplicationConfiguration configuration;

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            IConfirmationCodeGenerator generator, ICreditOrganizationRepository creditOrganizationRepository,
            ICreditApplicationDecisionRepository decisionRepository, ICreditApplicationChangelogRepository changelogRepository,
            ICreditOrganizationClient client, ApplicationConfiguration configuration, IEventPublisher eventPublisher, 
            ICommandSender commandSender)
        {
            this.logger = logger;
            this.repository = repository;
            this.eventPublisher = eventPublisher;
            this.commandSender = commandSender;
            this.generator = generator;
            this.creditOrganizationRepository = creditOrganizationRepository;
            this.decisionRepository = decisionRepository;
            this.changelogRepository = changelogRepository;
            this.client = client;
            this.configuration = configuration;
        }

        public async Task<Response<PagedResult<CreditApplicationListItemModel>>> GetCreditApplications(CreditApplicationListFilterModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplications = await repository.GetCreditApplications(model);
                logger.LogDebug($"Getting credit applications. Duration: {beginTime.GetDuration()} Request: {model} Count: {creditApplications.Items.Count}");
                return Response<PagedResult<CreditApplicationListItemModel>>.Ok(creditApplications);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit applications. Duration: {beginTime.GetDuration()} Request: {model}");
                return Response<PagedResult<CreditApplicationListItemModel>>.InternalServerError();
            }
        }

        public async Task<Response<List<CreditApplicationListItemExportModel>>> GetCreditApplicationsForExport(CreditApplicationListFilterModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplications = await repository.GetCreditApplicationsForExport(model);
                logger.LogDebug($"Getting credit applications for export. Duration: {beginTime.GetDuration()} Request: {model} Count: {creditApplications.Count}");
                return Response<List<CreditApplicationListItemExportModel>>.Ok(creditApplications);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit applications for export. Duration: {beginTime.GetDuration()} Request: {model}");
                return Response<List<CreditApplicationListItemExportModel>>.InternalServerError();
            }
        }

        public async Task<Response<CreditApplicationViewModel>> GetCreditApplicationViewModel(Guid creditApplicationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplication = await repository.GetCreditApplicationViewModel(creditApplicationId);
                creditApplication.CreditApplicationDecisions = await decisionRepository.GetCreditApplicationDecisions(creditApplicationId);
                creditApplication.CreditApplicationChanges = await changelogRepository.GetCreditApplicationChanges(creditApplicationId);
                
                logger.LogDebug($"Getting credit application view model. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Response: {creditApplication}");
                return Response<CreditApplicationViewModel>.Ok(creditApplication);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit application view model. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                return Response<CreditApplicationViewModel>.InternalServerError();
            }
        }

        public async Task<Response<CreditApplicationEditViewModel>> GetCreditApplicationEditViewModel(Guid creditApplicationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplication = await repository.GetCreditApplicationEditViewModel(creditApplicationId);
                
                logger.LogDebug($"Getting credit application edit view model. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Response: {creditApplication}");
                return Response<CreditApplicationEditViewModel>.Ok(creditApplication);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit application edit view model. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                return Response<CreditApplicationEditViewModel>.InternalServerError();
            }
        }

        public async Task<Response<List<CreditApplicationDecisionModel>>> GetCreditApplicationDecisions(Guid creditApplicationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplicationDecisions = await decisionRepository.GetCreditApplicationDecisions(creditApplicationId);
                logger.LogDebug($"Getting credit application decisions. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                return Response<List<CreditApplicationDecisionModel>, ErrorModel>.Ok(creditApplicationDecisions);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit application decisions. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                return Response<List<CreditApplicationDecisionModel>, ErrorModel>.InternalServerError();
            }
        }

        public async Task<Response<EmptyModel, ErrorModel>> ConfirmCreditApplication(Guid creditApplicationId, Guid creditOrganizationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var endpoint = await creditOrganizationRepository.GetCreditOrganizationEndpoint(creditOrganizationId);
                if (string.IsNullOrWhiteSpace(endpoint))
                {
                    logger.LogWarning($"ConfirmCreditApplication. Endpoint for credit organization not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(new ErrorModel{Message = "Кредитная организация не найдена"});
                }

                var request = await repository.GetCreditApplicationRequest(creditApplicationId);
                if (request == null)
                {
                    logger.LogWarning($"ConfirmCreditApplication. Credit application not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(new ErrorModel{Message = "Заявка на кредит не найдена"});
                }

                request.CreditOrganizationId = creditOrganizationId;
                var response = await client.ConfirmCreditApplication(endpoint, creditApplicationId, request);
                if (response.IsSuccessStatusCode)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Credit application confirmation sent. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}, Endpoint: {endpoint}, Response: {response.Content}");
                    await decisionRepository.CreateOrUpdateCreditApplicationDecision(creditApplicationId, response.Content.Date, response.Content.CreditApplications);
                }
                else
                {
                    logger.LogError($"ConfirmCreditApplication. Credit application confirmation error. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}, Endpoint: {endpoint}, ErrorMessage: {response.ErrorMessage}");
                    return Response<EmptyModel, ErrorModel>.InternalServerError();
                }

                return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while confiming credit application. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}. CreditOrganizationId: {creditOrganizationId}");
                return Response<EmptyModel, ErrorModel>.InternalServerError();
            }
        }

        public async Task<Response<EmptyModel, ErrorModel>> ResendCreditApplicationRequest(Guid creditApplicationId, Guid creditOrganizationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var endpoint = await creditOrganizationRepository.GetCreditOrganizationEndpoint(creditOrganizationId);
                if (string.IsNullOrWhiteSpace(endpoint))
                {
                    logger.LogWarning($"ResendCreditApplicationRequest. Endpoint for credit organization not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(new ErrorModel{Message = "Кредитная организация не найдена"});
                }

                var request = await repository.GetCreditApplicationRequest(creditApplicationId);
                if (request == null)
                {
                    logger.LogWarning($"ResendCreditApplicationRequest. Credit application not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(new ErrorModel{Message = "Заявка на кредит не найдена"});
                }

                var response = await client.ResendCreditApplicationRequest(endpoint, creditApplicationId, request);
                if (response.IsSuccessStatusCode)
                {
                    logger.LogInformation($"ResendCreditApplicationRequest. Credit application status received. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}, Response: {response.Content}");
                    await decisionRepository.CreateOrUpdateCreditApplicationDecision(creditApplicationId, response.Content.Date, response.Content.CreditApplications);
                }
                else
                {
                    logger.LogError($"ResendCreditApplicationRequest. Get credit application status error. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}, ErrorMessage: {response.ErrorMessage}");
                    return Response<EmptyModel, ErrorModel>.InternalServerError();
                }
               
                return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit application status. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}. CreditOrganizationId: {creditOrganizationId}");
                return Response<EmptyModel, ErrorModel>.InternalServerError();
            }
        }

        public async Task<Response> ChangeCreditApplicationStatus(Guid creditApplicationId, ChangeCreditApplicationStatusModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                await repository.ChangeCreditApplicationStatus(creditApplicationId, model);
                sendAddChangelogCommand(creditApplicationId, model.Status, model.Comment);
                logger.LogInformation($"Credit application status successfully changed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                return Response.Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while changing credit application status. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                return Response.InternalServerError();
            }
        }

        public async Task<Response> UpdateCreditApplication(Guid creditApplicationId, UpdateCreditApplicationModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                await repository.UpdateCreditApplication(creditApplicationId, model);
                //sendAddChangelogCommand(creditApplicationId, model.Status, model.Comment);//todo status change?
                logger.LogInformation($"Credit application successfully updated. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                return Response.Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while updating credit application. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                return Response.InternalServerError();
            }
        }

        public async Task<Response> DeleteExpiredCreditApplications()
        {
            var beginTime = DateTime.Now;
            try
            {
                var date = DateTime.Now - configuration.NotConfirmedCreditApplicationsLifetime;
                var deletedCount = await repository.DeleteExpiredCreditApplications(date);
                logger.LogInformation($"{deletedCount} expired credit applications deleted. Duration: {beginTime.GetDuration()}");
                return Response.Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while deleting expired credit applications. Duration: {beginTime.GetDuration()}");
                return Response.InternalServerError();
            }
        }

        private void sendAddChangelogCommand(Guid creditApplicationId, CreditApplicationStatus status, string comment = null)
        {
            var command = new AddCreditApplicationChangelogRecordCommand
            {
                CreditApplicationId = creditApplicationId,
                Date = DateTime.Now,
                Status = status,
                Comment = comment
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Changes);
        }
    }
}