﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Data.Public.ShortProfile;
using CreditApplication.Api.Extensions;
using CreditApplication.Api.Models.Configuration;
using CreditApplication.Api.Models.Enums;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Public.ShortProfile;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;

namespace CreditApplication.Api.Domain.Public.ShortProfile
{
    public interface ICreditApplicationManager : IBaseCreditApplicationManager
    {
        Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model);
        new Task<Response<EmptyModel, ErrorModel>> ConfirmPhoneNumber(Guid creditApplicationId, string confirmationCode);
    }

    public class CreditApplicationManager : BaseCreditApplicationManager, ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly IEventPublisher eventPublisher;
        private readonly IConfirmationCodeGenerator generator;
        private readonly ApplicationConfiguration configuration;

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            IConfirmationCodeGenerator generator, ApplicationConfiguration configuration, IEventPublisher eventPublisher, 
            ICommandSender commandSender) : base(logger, repository, generator, configuration, commandSender)
        {
            this.logger = logger;
            this.repository = repository;
            this.eventPublisher = eventPublisher;
            this.generator = generator;
            this.configuration = configuration;
        }

        public async Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var confirmationCode = generator.GenerateCode();
                var creditApplication = await repository.CreateCreditApplication(model, confirmationCode);
                SendSms(model.PhoneNumber, confirmationCode);
                SendAddChangelogCommand(creditApplication.CreditApplicationId, CreditApplicationStatus.Draft);
                
                logger.LogInformation($"Credit application successfully created. Duration: {beginTime.GetDuration()} Request: {model}, ConfirmationCode: {confirmationCode}, SystemInfo: {creditApplication}");
                return Response<Guid>.Ok(creditApplication.CreditApplicationId);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while creating credit application. Duration: {beginTime.GetDuration()} Request: {model}");
                return Response<Guid>.InternalServerError();
            }
        }
        
        public new async Task<Response<EmptyModel, ErrorModel>> ConfirmPhoneNumber(Guid creditApplicationId, string confirmationCode)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplication = await repository.GetCreditApplicationDraft(creditApplicationId);
                if (creditApplication == null)
                {
                    logger.LogWarning($"ConfirmPhoneNumber. Credit application not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.NotFound();
                }
                if (creditApplication.Status != CreditApplicationStatus.Draft)
                {
                    logger.LogWarning($"ConfirmPhoneNumber. Credit application in status {creditApplication.Status}. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.CreditApplicationInvalidStatus.ToErrorModel());
                }
                if (creditApplication.Step >= CreditApplicationStep.ConfirmationCode)
                {
                    logger.LogInformation($"ConfirmPhoneNumber. Phone number {creditApplication.PhoneNumber} already confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
                }
                
                if (confirmationCode != creditApplication.ConfirmationCode)
                {
                    logger.LogInformation($"Confirmation code {confirmationCode} does not match with {creditApplication.ConfirmationCode}. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, PhoneNumber: {creditApplication.PhoneNumber}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(new ErrorModel {Message = "Неверный код подтверждения"});
                }
               
                if (DateTime.Now - creditApplication.CreatedDate > configuration.ConfirmationCodeLifetime)
                {
                    logger.LogInformation($"ConfirmPhoneNumber. Confirmation code expired. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.ConfirmationCodeExpired.ToErrorModel());
                }
                    
                var @event = await repository.ConfirmPhoneNumber(creditApplicationId);
                eventPublisher.PublishEvent(@event);
                SendAddChangelogCommand(creditApplicationId, CreditApplicationStatus.New);
                
                logger.LogInformation($"Phone number {creditApplication.PhoneNumber} successfully confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while confirming phone number. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                return Response<EmptyModel, ErrorModel>.InternalServerError();
            }
        }
    }
}