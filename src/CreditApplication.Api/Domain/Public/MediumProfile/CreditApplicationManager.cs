﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Data.Public.MediumProfile;
using CreditApplication.Api.Extensions;
using CreditApplication.Api.Models.Configuration;
using CreditApplication.Api.Models.Enums;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Public.MediumProfile;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;

namespace CreditApplication.Api.Domain.Public.MediumProfile
{
    public interface ICreditApplicationManager : IBaseCreditApplicationManager
    {
        Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model);
        Task<Response<EmptyModel, ErrorModel>> SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model);
    }

    public class CreditApplicationManager : BaseCreditApplicationManager, ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly IEventPublisher eventPublisher;
        private readonly IConfirmationCodeGenerator generator;

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            IConfirmationCodeGenerator generator, ApplicationConfiguration configuration, IEventPublisher eventPublisher, 
            ICommandSender commandSender) : base(logger, repository, generator, configuration, commandSender)
        {
            this.logger = logger;
            this.repository = repository;
            this.eventPublisher = eventPublisher;
            this.generator = generator;
        }

        public async Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var confirmationCode = generator.GenerateCode();
                var creditApplication = await repository.CreateCreditApplication(model, confirmationCode);
                SendSms(model.PhoneNumber, confirmationCode);
                SendAddChangelogCommand(creditApplication.CreditApplicationId, CreditApplicationStatus.Draft);
                
                logger.LogInformation($"Credit application successfully created. Duration: {beginTime.GetDuration()} Request: {model}, ConfirmationCode: {confirmationCode}, SystemInfo: {creditApplication}");
                return Response<Guid>.Ok(creditApplication.CreditApplicationId);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while creating credit application. Duration: {beginTime.GetDuration()} Request: {model}");
                return Response<Guid>.InternalServerError();
            }
        }

        public async Task<Response<EmptyModel, ErrorModel>> SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var confirmationCodeModel = await repository.GetCreditApplicationDraft(creditApplicationId);
                if (confirmationCodeModel == null)
                {
                    logger.LogInformation($"SaveCreditApplicationAdditionalInformation. Credit application not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                    return Response<EmptyModel, ErrorModel>.NotFound();
                }
                if (confirmationCodeModel.Status != CreditApplicationStatus.Draft)
                {
                    logger.LogWarning($"SaveCreditApplicationAdditionalInformation. Credit application in status {confirmationCodeModel.Status}. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.CreditApplicationInvalidStatus.ToErrorModel());
                }
                if (confirmationCodeModel.Step < CreditApplicationStep.ConfirmationCode)
                {
                    logger.LogInformation($"SaveCreditApplicationAdditionalInformation. Phone number {confirmationCodeModel.PhoneNumber} not confirmed yet. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.PhoneNumberNotConfirmed.ToErrorModel());
                }
                
                var @event = await repository.SaveCreditApplicationAdditionalInformation(creditApplicationId, model);
                eventPublisher.PublishEvent(@event);
                SendAddChangelogCommand(creditApplicationId, CreditApplicationStatus.New);
                
                logger.LogInformation($"Client additional information successfully saved. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while saving client additional information. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                return Response<EmptyModel, ErrorModel>.InternalServerError();
            }
        }
    }
}