﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Data.Public;
using CreditApplication.Api.Extensions;
using CreditApplication.Api.Models.Commands;
using CreditApplication.Api.Models.Configuration;
using CreditApplication.Api.Models.Enums;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;
using Yes.Sms.Api.Contracts.Commands;

namespace CreditApplication.Api.Domain.Public
{
    public interface IBaseCreditApplicationManager
    {
        Task<Response<EmptyModel, ErrorModel>> ResendConfirmationCode(Guid creditApplicationId);
        Task<Response<EmptyModel, ErrorModel>> ConfirmPhoneNumber(Guid creditApplicationId, string confirmationCode);
    }

    public class BaseCreditApplicationManager : IBaseCreditApplicationManager
    {
        private readonly ILogger<BaseCreditApplicationManager> logger;
        private readonly IBaseCreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly IConfirmationCodeGenerator generator;
        private readonly ApplicationConfiguration configuration;

        public BaseCreditApplicationManager(ILogger<BaseCreditApplicationManager> logger, 
            IBaseCreditApplicationRepository repository, IConfirmationCodeGenerator generator, 
            ApplicationConfiguration configuration, ICommandSender commandSender)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.generator = generator;
            this.configuration = configuration;
        }

        public async Task<Response<EmptyModel, ErrorModel>> ResendConfirmationCode(Guid creditApplicationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplication = await repository.GetCreditApplicationDraft(creditApplicationId);
                if (creditApplication == null)
                {
                    logger.LogWarning($"ResendConfirmationCode. Credit application not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                    return Response<EmptyModel, ErrorModel>.NotFound();
                }
                if (creditApplication.Status != CreditApplicationStatus.Draft)
                {
                    logger.LogWarning($"ResendConfirmationCode. Credit application in status {creditApplication.Status}. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.CreditApplicationInvalidStatus.ToErrorModel());
                }
                if (creditApplication.Step >= CreditApplicationStep.ConfirmationCode)
                {
                    logger.LogWarning($"ResendConfirmationCode. Phone number {creditApplication.PhoneNumber} already confirmed. Skip request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                    return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
                }
                if (DateTime.Now - creditApplication.CreatedDate < configuration.ResendSmsPeriod)
                {
                    logger.LogWarning($"ResendConfirmationCode. {configuration.ResendSmsPeriod.TotalSeconds} seconds not passed since sending previous confirmation code. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.ResendSmsPeriodNotPassed.ToErrorModel());
                }
                
                var confirmationCode = generator.GenerateCode();
                SendSms(creditApplication.PhoneNumber, confirmationCode);
                
                await repository.UpdateConfirmationCode(creditApplicationId, confirmationCode);
                logger.LogInformation($"Confirmation code succsessfully sended. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}, PhoneNumber: {creditApplication.PhoneNumber}");
                return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while resending confirmation code. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                return Response<EmptyModel, ErrorModel>.InternalServerError();
            }
        }

        public async Task<Response<EmptyModel, ErrorModel>> ConfirmPhoneNumber(Guid creditApplicationId, string confirmationCode)
        {
            var beginTime = DateTime.Now;
            try
            {
                var creditApplication = await repository.GetCreditApplicationDraft(creditApplicationId);
                if (creditApplication == null)
                {
                    logger.LogWarning($"ConfirmPhoneNumber. Credit application not found. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.NotFound();
                }
                if (creditApplication.Status != CreditApplicationStatus.Draft)
                {
                    logger.LogWarning($"ConfirmPhoneNumber. Credit application in status {creditApplication.Status}. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.CreditApplicationInvalidStatus.ToErrorModel());
                }
                if (creditApplication.Step >= CreditApplicationStep.ConfirmationCode)
                {
                    logger.LogInformation($"ConfirmPhoneNumber. Phone number {creditApplication.PhoneNumber} already confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
                }
                
                if (confirmationCode != creditApplication.ConfirmationCode)
                {
                    logger.LogInformation($"Confirmation code {confirmationCode} does not match with {creditApplication.ConfirmationCode}. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, PhoneNumber: {creditApplication.PhoneNumber}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(new ErrorModel {Message = "Неверный код подтверждения"});
                }
               
                if (DateTime.Now - creditApplication.CreatedDate > configuration.ConfirmationCodeLifetime)
                {
                    logger.LogInformation($"ConfirmPhoneNumber. Confirmation code expired. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                    return Response<EmptyModel, ErrorModel>.BadRequest(ErrorCode.ConfirmationCodeExpired.ToErrorModel());
                }
                    
                await repository.ConfirmPhoneNumber(creditApplicationId);
                logger.LogInformation($"Phone number {creditApplication.PhoneNumber} successfully confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                return Response<EmptyModel, ErrorModel>.Ok(new EmptyModel());
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while confirming phone number. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ConfirmationCode: {confirmationCode}");
                return Response<EmptyModel, ErrorModel>.InternalServerError();
            }
        }

        protected void SendSms(string phoneNumber, string confirmationCode)
        {
            var command = new SendSmsCommand
            {
                PnoneNumber = phoneNumber,
                Message = configuration.ConfirmationCodeSmsTemplate.Replace("{CONFIRMATION_CODE}", confirmationCode)
            };
            commandSender.SendCommand(command, BoundedContexts.SMS_API);
        }
        
        protected void SendAddChangelogCommand(Guid creditApplicationId, CreditApplicationStatus status, string comment = null)
        {
            var command = new AddCreditApplicationChangelogRecordCommand
            {
                CreditApplicationId = creditApplicationId,
                Date = DateTime.Now,
                Status = status,
                Comment = comment
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Changes);
        }
    }
}