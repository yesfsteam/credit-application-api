﻿using System;

namespace CreditApplication.Api.Domain
{
    public interface IConfirmationCodeGenerator
    {
        string GenerateCode();
    }
    
    public class ConfirmationCodeGenerator : IConfirmationCodeGenerator
    {
        public string GenerateCode()
        {
            var random = new Random();
            return random.Next(0, 9999).ToString("D4");
        }
    }
}