﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace CreditApplication.Api.Domain
{
    public interface ICreditOrganizationClient
    {
        /// <summary>
        /// Отправляет заново запрос на кредит
        /// </summary>
        Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(string uri, Guid creditApplicationId, CreditApplicationRequest request);
        
        /// <summary>
        /// Подтверждает заявку на кредит
        /// </summary>
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(string uri, Guid creditApplicationId, ConfirmCreditApplicationRequest request);
    }

    public class CreditOrganizationClient : RestClientBase, ICreditOrganizationClient
    {
        public CreditOrganizationClient(HttpClient client) : base(client)
        {
        }

        public async Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(string uri, Guid creditApplicationId, CreditApplicationRequest request)
        {
            return await Post<CreditApplicationResponse>(new Uri(new Uri(uri), $"api/v1/credit-applications/{creditApplicationId}/retry").ToString(), request);
        } 
        
        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(string uri, Guid creditApplicationId, ConfirmCreditApplicationRequest request)
        {
            return await Post<CreditApplicationResponse>(new Uri(new Uri(uri), $"api/v1/credit-applications/{creditApplicationId}/confirmation").ToString(), request);
        } 
    }
}