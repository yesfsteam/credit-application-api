﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Data;
using CreditApplication.Api.Models.Commands;
using Microsoft.Extensions.Logging;
using SD.Messaging.Models;
using Yes.Infrastructure.Common.Extensions;

namespace CreditApplication.Api.Domain
{
    public interface ICreditApplicationChangelogManager
    {
        Task<ProcessingResult> ProcessCreditApplicationChange(AddCreditApplicationChangelogRecordCommand command);
    }

    public class CreditApplicationChangelogManager : ICreditApplicationChangelogManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationChangelogRepository repository;

        public CreditApplicationChangelogManager(ILogger<CreditApplicationManager> logger, ICreditApplicationChangelogRepository repository)
        {
            this.logger = logger;
            this.repository = repository;
        }

        public async Task<ProcessingResult> ProcessCreditApplicationChange(AddCreditApplicationChangelogRecordCommand command)
        {
            var beginTime = DateTime.Now;
            try
            {
                await repository.CreateCreditApplicationChangelogRecord(command);
                logger.LogDebug($"Credit application change created. Duration: {beginTime.GetDuration()} Command: {command}");
                return ProcessingResult.Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while processing credit application change. Duration: {beginTime.GetDuration()} Command: {command}");
                return ProcessingResult.Fail();
            }
        }
    }
}