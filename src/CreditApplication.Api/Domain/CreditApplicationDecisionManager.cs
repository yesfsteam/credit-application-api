﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CreditApplication.Api.Data;
using Microsoft.Extensions.Logging;
using SD.Messaging.Models;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace CreditApplication.Api.Domain
{
    public interface ICreditApplicationDecisionManager
    {
        Task<ProcessingResult> ProcessCreditApplicationDecision(CreditApplicationDecisionsCommand command);
    }

    public class CreditApplicationDecisionManager : ICreditApplicationDecisionManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationDecisionRepository repository;
        private readonly ICreditApplicationManager creditApplicationManager;

        public CreditApplicationDecisionManager(ILogger<CreditApplicationManager> logger, ICreditApplicationDecisionRepository repository, ICreditApplicationManager creditApplicationManager)
        {
            this.logger = logger;
            this.repository = repository;
            this.creditApplicationManager = creditApplicationManager;
        }

        public async Task<ProcessingResult> ProcessCreditApplicationDecision(CreditApplicationDecisionsCommand command)
        {
            var beginTime = DateTime.Now;
            try
            {
                var decisions = await repository.CreateOrUpdateCreditApplicationDecisionAndReturnAllStatuses(command.CreditApplicationId, command.Date, command.CreditApplications);
                if (decisions.All(x => x == CreditOrganizationRequestStatus.Confirmed || x == CreditOrganizationRequestStatus.NotApplicable))
                {
                    await creditApplicationManager.ChangeCreditApplicationStatus(command.CreditApplicationId, new ChangeCreditApplicationStatusModel
                    {
                        Status = CreditApplicationStatus.Processed,
                        Comment = "Заявка закрыта автоматически"
                    });
                }
                logger.LogInformation($"Credit application decision successfully created. Duration: {beginTime.GetDuration()} Command: {command}");
                return ProcessingResult.Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while processing credit application decision. Duration: {beginTime.GetDuration()} Command: {command}");
                return ProcessingResult.Fail();
            }
        }
    }
}