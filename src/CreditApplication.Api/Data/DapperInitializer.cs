﻿using Dapper;

namespace CreditApplication.Api.Data
{
    public static class DapperInitializer
    {
        public static void ConfigureDapper()
        {
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }
    }
}