﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Models.Inner;
using Dapper;
using Microsoft.Extensions.Configuration;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.CreditApplication.Api.Contracts.Public.ShortProfile;

namespace CreditApplication.Api.Data.Public.ShortProfile
{
    public interface ICreditApplicationRepository : IBaseCreditApplicationRepository
    {
        Task<CreditApplicationCreatedModel> CreateCreditApplication(CreateCreditApplicationModel model, string confirmationCode);
        new Task<CreditApplicationCreatedEvent> ConfirmPhoneNumber(Guid creditApplicationId);
    }

    public class CreditApplicationRepository : BaseCreditApplicationRepository, ICreditApplicationRepository
    {
        public CreditApplicationRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<CreditApplicationCreatedModel> CreateCreditApplication(CreateCreditApplicationModel model, string confirmationCode)
        {
            var response = new CreditApplicationCreatedModel
            {
                CreditApplicationId = Guid.NewGuid(),
                CreatedDate = DateTime.Now
            };
            var queryParams = new
            {
                response.CreditApplicationId,
                response.CreatedDate,
                profileType = ProfileType.Short,
                status = CreditApplicationStatus.Draft,
                model.PartnerId,
                step = CreditApplicationStep.MainInformation,
                model.LastName,
                model.FirstName,
                model.MiddleName,
                model.DateOfBirth,
                model.PhoneNumber,
                model.AdditionalPhoneNumber,
                model.ResidenceAddressRegion,
                model.ResidenceAddressRegionKladrCode,
                confirmationCode,
                model.RequestIp,
                personalDataProcessApproved = false,
                creditBureauProcessApproved = false
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application (
                   id, 
                   created_date,
                   profile_type,
                   date,
                   status,
                   partner_id,
                   step,
                   last_name, 
                   first_name, 
                   middle_name, 
                   date_of_birth,
                   phone_number, 
                   additional_phone_number, 
                   residence_address_region, 
                   residence_address_region_kladr_code, 
                   confirmation_code,
                   request_ip,
                   personal_data_process_approved,
                   credit_bureau_process_approved
                ) VALUES (
                   :creditApplicationId::uuid,
                   :createdDate,
                   :profileType,
                   :createdDate,
                   :status,
                   :partnerId,
                   :step,
                   :lastName,
                   :firstName,
                   :middleName,
                   :dateOfBirth,
                   :phoneNumber,
                   :additionalPhoneNumber,
                   :residenceAddressRegion,
                   :residenceAddressRegionKladrCode,
                   :confirmationCode,
                   :requestIp,
                   :personalDataProcessApproved,
                   :creditBureauProcessApproved
                );", queryParams);
            return response;
        }

        public new async Task<CreditApplicationCreatedEvent> ConfirmPhoneNumber(Guid creditApplicationId)
        {
            var queryParams = new
            {
                status = CreditApplicationStatus.New,
                creditApplicationId,
                step = CreditApplicationStep.ConfirmationCode,
                date = DateTime.Now,
                personalDataProcessApproved = true,
                creditBureauProcessApproved = true
            };

            await using var connection = CreateConnection();
            return await connection.QueryFirstAsync<CreditApplicationCreatedEvent>(@"
                UPDATE credit_application
                SET
                   status = :status,
                   step = :step,
                   date = :date,
                   personal_data_process_approved = :personalDataProcessApproved,
                   personal_data_process_approve_date = :date,
                   credit_bureau_process_approved = :creditBureauProcessApproved,
                   credit_bureau_process_approve_date = :date
                WHERE
                    id = :creditApplicationId::uuid
                RETURNING 
                   id AS credit_application_id,
                   date,
                   profile_type,
                   partner_id,
                   last_name,
                   first_name,
                   middle_name,
                   phone_number,
                   confirmation_code,
                   personal_data_process_approved,
                   personal_data_process_approve_date,
                   credit_bureau_process_approved,
                   credit_bureau_process_approve_date,
                   confirmation_code,
                   date_of_birth,
                   residence_address_region,
                   residence_address_region_kladr_code,
                   additional_phone_number;", queryParams);
        }
    }
}