﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Models.Inner;
using Dapper;
using Microsoft.Extensions.Configuration;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.CreditApplication.Api.Contracts.Public.FullProfile;

namespace CreditApplication.Api.Data.Public.FullProfile
{
    public interface ICreditApplicationRepository : IBaseCreditApplicationRepository
    {
        Task<CreditApplicationCreatedModel> CreateCreditApplication(CreateCreditApplicationModel model, string confirmationCode);
        Task SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model);
        Task<CreditApplicationCreatedEvent> SaveCreditApplicationEmployerInformationAndConfirm(Guid creditApplicationId, CreditApplicationEmployerInformationModel model);
    }

    public class CreditApplicationRepository : BaseCreditApplicationRepository, ICreditApplicationRepository
    {
        public CreditApplicationRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<CreditApplicationCreatedModel> CreateCreditApplication(CreateCreditApplicationModel model, string confirmationCode)
        {
            var response = new CreditApplicationCreatedModel
            {
                CreditApplicationId = Guid.NewGuid(),
                CreatedDate = DateTime.Now
            };
            var queryParams = new
            {
                response.CreditApplicationId,
                response.CreatedDate,
                profileType = ProfileType.Full,
                status = CreditApplicationStatus.Draft,
                model.PartnerId,
                step = CreditApplicationStep.MainInformation,
                model.LastName,
                model.FirstName,
                model.MiddleName,
                model.PhoneNumber,
                model.Email,
                model.CreditPeriod,
                model.CreditAmount,
                confirmationCode,
                model.RequestIp,
                personalDataProcessApproved = false,
                creditBureauProcessApproved = false
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application (
                   id, 
                   created_date,
                   profile_type,
                   date,
                   status,
                   partner_id,
                   step,
                   last_name, 
                   first_name, 
                   middle_name, 
                   phone_number, 
                   email,
                   credit_period,
                   credit_amount,
                   confirmation_code,
                   request_ip,
                   personal_data_process_approved,
                   credit_bureau_process_approved
                ) VALUES (
                   :creditApplicationId::uuid,
                   :createdDate,
                   :profileType,
                   :createdDate,
                   :status,
                   :partnerId,
                   :step,
                   :lastName,
                   :firstName,
                   :middleName,
                   :phoneNumber,
                   :email,
                   :creditPeriod,
                   :creditAmount,
                   :confirmationCode,
                   :requestIp,
                   :personalDataProcessApproved,
                   :creditBureauProcessApproved
                );", queryParams);
            return response;
        }

        public async Task SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model)
        {
            var queryParams = new
            {
                creditApplicationId,
                step = CreditApplicationStep.AdditionalInformation,
                date = DateTime.Now,
                model.DateOfBirth,
                model.PlaceOfBirth,
                model.Gender,
                model.PassportSeries,
                model.PassportNumber,
                model.PassportIssueDate,
                model.PassportIssuer,
                model.PassportDepartmentCode,
                model.RegistrationAddressRegion,
                model.RegistrationAddressRegionKladrCode,
                model.RegistrationAddressCity,
                model.RegistrationAddressCityKladrCode,
                model.RegistrationAddressStreet,
                model.RegistrationAddressStreetKladrCode,
                model.RegistrationAddressHouse,
                model.RegistrationAddressBlock,
                model.RegistrationAddressBuilding,
                model.RegistrationAddressApartment,
                model.RegistrationAddressKladrCode,
                model.ResidenceAddressRegion,
                model.ResidenceAddressRegionKladrCode,
                model.ResidenceAddressCity,
                model.ResidenceAddressCityKladrCode,
                model.ResidenceAddressStreet,
                model.ResidenceAddressStreetKladrCode,
                model.ResidenceAddressHouse,
                model.ResidenceAddressBlock,
                model.ResidenceAddressBuilding,
                model.ResidenceAddressApartment,
                model.ResidenceAddressKladrCode,
                model.Education,
                model.MaritalStatus,
                model.DependentsCount,
                model.ConfirmationDocument,
                model.HasFlat,
                model.HasHouse,
                model.HasArea,
                model.HasCar,
                model.AdditionalPhoneNumberType,
                model.AdditionalPhoneNumber
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                UPDATE credit_application
                SET
                   step = :step,
                   date = :date,
                   date_of_birth = :dateOfBirth,
                   place_of_birth = :placeOfBirth,
                   gender = :gender,
                   passport_series = :passportSeries,
                   passport_number = :passportNumber,
                   passport_issuer = :passportIssuer,
                   passport_issue_date = :passportIssueDate,
                   passport_department_code = :passportDepartmentCode,
                   registration_address_region = :registrationAddressRegion,
                   registration_address_region_kladr_code = :registrationAddressRegionKladrCode,
                   registration_address_city = :registrationAddressCity,
                   registration_address_city_kladr_code = :registrationAddressCityKladrCode,
                   registration_address_street = :registrationAddressStreet,
                   registration_address_street_kladr_code = :registrationAddressStreetKladrCode,
                   registration_address_house = :registrationAddressHouse,
                   registration_address_block = :registrationAddressBlock,
                   registration_address_building = :registrationAddressBuilding,
                   registration_address_apartment = :registrationAddressApartment,
                   registration_address_kladr_code = :registrationAddressKladrCode,
                   residence_address_region = :residenceAddressRegion,
                   residence_address_region_kladr_code = :residenceAddressRegionKladrCode,
                   residence_address_city = :residenceAddressCity,
                   residence_address_city_kladr_code = :residenceAddressCityKladrCode,
                   residence_address_street = :residenceAddressStreet,
                   residence_address_street_kladr_code = :residenceAddressStreetKladrCode,
                   residence_address_house = :residenceAddressHouse,
                   residence_address_block = :residenceAddressBlock,
                   residence_address_building = :residenceAddressBuilding,
                   residence_address_apartment = :residenceAddressApartment,
                   residence_address_kladr_code = :residenceAddressKladrCode,
                   education = :education,
                   marital_status = :maritalStatus,
                   dependents_count = :dependentsCount,
                   confirmation_document = :confirmationDocument,
                   has_flat = :hasFlat,
                   has_house = :hasHouse,
                   has_area = :hasArea,
                   has_car = :hasCar,
                   additional_phone_number_type = :additionalPhoneNumberType,
                   additional_phone_number = :additionalPhoneNumber
                WHERE
                    id = :creditApplicationId::uuid;", queryParams);
        }

        public async Task<CreditApplicationCreatedEvent> SaveCreditApplicationEmployerInformationAndConfirm(Guid creditApplicationId, CreditApplicationEmployerInformationModel model)
        {
            var queryParams = new
            {
                creditApplicationId,
                status = CreditApplicationStatus.New,
                step = CreditApplicationStep.EmployerInformation,
                date = DateTime.Now,
                model.Activity,
                model.Tin,
                model.MonthlyIncome,
                model.EmployerName,
                model.EmployerAddressRegion,
                model.EmployerAddressRegionKladrCode,
                model.EmployerAddressCity,
                model.EmployerAddressCityKladrCode,
                model.EmployerAddressStreet,
                model.EmployerAddressStreetKladrCode,
                model.EmployerAddressHouse,
                model.EmployerAddressBlock,
                model.EmployerAddressBuilding,
                model.EmployerAddressApartment,
                model.EmployerAddressKladrCode,
                model.EmployerIndustry,
                model.EmployerStaff,
                model.EmployerPhoneNumber,
                model.EmployeeStartDate,
                model.EmployeePosition
            };

            await using var connection = CreateConnection();
            return await connection.QueryFirstAsync<CreditApplicationCreatedEvent>(@"
                UPDATE credit_application
                SET
                   status = :status,
                   step = :step,
                   date = :date,
                   activity = :activity,
                   tin = :tin,
                   monthly_income = :monthlyIncome,
                   employer_name = :employerName,
                   employer_address_region = :employerAddressRegion,
                   employer_address_region_kladr_code = :employerAddressRegionKladrCode,
                   employer_address_city = :employerAddressCity,
                   employer_address_city_kladr_code = :employerAddressCityKladrCode,
                   employer_address_street = :employerAddressStreet,
                   employer_address_street_kladr_code = :employerAddressStreetKladrCode,
                   employer_address_house = :employerAddressHouse,
                   employer_address_block = :employerAddressBlock,
                   employer_address_building = :employerAddressBuilding,
                   employer_address_apartment = :employerAddressApartment,
                   employer_address_kladr_code = :employerAddressKladrCode,
                   employer_industry = :employerIndustry,
                   employer_staff = :employerStaff,
                   employer_phone_number = :employerPhoneNumber,
                   employee_start_date = :employeeStartDate,
                   employee_position = :employeePosition                   
                WHERE
                    id = :creditApplicationId::uuid
                RETURNING 
                   id AS credit_application_id,
                   date,
                   profile_type,
                   partner_id,
                   last_name,
                   first_name,
                   middle_name,
                   phone_number,
                   email,
                   credit_period,
                   credit_amount,
                   confirmation_code,
                   personal_data_process_approved,
                   personal_data_process_approve_date,
                   credit_bureau_process_approved,
                   credit_bureau_process_approve_date,
                   confirmation_code,
                   date_of_birth,
                   place_of_birth,
                   gender,
                   passport_series,
                   passport_number,
                   passport_issuer,
                   passport_issue_date,
                   passport_department_code,
                   registration_address_region,
                   registration_address_region_kladr_code,
                   registration_address_city,
                   registration_address_city_kladr_code,
                   registration_address_street,
                   registration_address_street_kladr_code,
                   registration_address_house,
                   registration_address_block,
                   registration_address_building,
                   registration_address_apartment,
                   registration_address_kladr_code,
                   residence_address_region,
                   residence_address_region_kladr_code,
                   residence_address_city,
                   residence_address_city_kladr_code,
                   residence_address_street,
                   residence_address_street_kladr_code,
                   residence_address_house,
                   residence_address_block,
                   residence_address_building,
                   residence_address_apartment,
                   residence_address_kladr_code,
                   education,
                   marital_status,
                   dependents_count,
                   confirmation_document,
                   has_flat,
                   has_house,
                   has_area,
                   has_car,
                   additional_phone_number_type,
                   additional_phone_number,
                   activity,
                   tin,
                   monthly_income,
                   employer_name,
                   employer_address_region,
                   employer_address_region_kladr_code,
                   employer_address_city,
                   employer_address_city_kladr_code,
                   employer_address_street,
                   employer_address_street_kladr_code,
                   employer_address_house,
                   employer_address_block,
                   employer_address_building,
                   employer_address_apartment,
                   employer_address_kladr_code,
                   employer_industry,
                   employer_staff,
                   employer_phone_number,
                   employee_start_date,
                   employee_position", queryParams);
        }
    }
}