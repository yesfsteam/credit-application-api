﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Models.Inner;
using Dapper;
using Microsoft.Extensions.Configuration;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.CreditApplication.Api.Contracts.Public.MediumProfile;

namespace CreditApplication.Api.Data.Public.MediumProfile
{
    public interface ICreditApplicationRepository : IBaseCreditApplicationRepository
    {
        Task<CreditApplicationCreatedModel> CreateCreditApplication(CreateCreditApplicationModel model, string confirmationCode);
        Task<CreditApplicationCreatedEvent> SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model);
    }

    public class CreditApplicationRepository : BaseCreditApplicationRepository, ICreditApplicationRepository
    {
        public CreditApplicationRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<CreditApplicationCreatedModel> CreateCreditApplication(CreateCreditApplicationModel model, string confirmationCode)
        {
            var response = new CreditApplicationCreatedModel
            {
                CreditApplicationId = Guid.NewGuid(),
                CreatedDate = DateTime.Now
            };
            var queryParams = new
            {
                response.CreditApplicationId,
                response.CreatedDate,
                profileType = ProfileType.Medium,
                status = CreditApplicationStatus.Draft,
                model.PartnerId,
                step = CreditApplicationStep.MainInformation,
                model.LastName,
                model.FirstName,
                model.MiddleName,
                model.PhoneNumber,
                model.CreditPeriod,
                model.CreditAmount,
                confirmationCode,
                model.RequestIp,
                personalDataProcessApproved = false,
                creditBureauProcessApproved = false
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application (
                   id, 
                   created_date,
                   profile_type,
                   date,
                   status,
                   partner_id,
                   step,
                   last_name, 
                   first_name, 
                   middle_name, 
                   phone_number, 
                   credit_period,
                   credit_amount,
                   confirmation_code,
                   request_ip,
                   personal_data_process_approved,
                   credit_bureau_process_approved
                ) VALUES (
                   :creditApplicationId::uuid,
                   :createdDate,
                   :profileType,
                   :createdDate,
                   :status,
                   :partnerId,
                   :step,
                   :lastName,
                   :firstName,
                   :middleName,
                   :phoneNumber,
                   :creditPeriod,
                   :creditAmount,
                   :confirmationCode,
                   :requestIp,
                   :personalDataProcessApproved,
                   :creditBureauProcessApproved
                );", queryParams);
            return response;
        }

        public async Task<CreditApplicationCreatedEvent> SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model)
        {
            var queryParams = new
            {
                creditApplicationId,
                status = CreditApplicationStatus.New,
                step = CreditApplicationStep.AdditionalInformation,
                date = DateTime.Now,
                model.DateOfBirth,
                model.PlaceOfBirth,
                model.Gender,
                model.MonthlyIncome,
                model.PassportSeries,
                model.PassportNumber,
                model.PassportIssueDate,
                model.PassportIssuer,
                model.PassportDepartmentCode,
                model.RegistrationAddressRegion,
                model.RegistrationAddressRegionKladrCode,
                model.RegistrationAddressCity,
                model.RegistrationAddressCityKladrCode,
                model.RegistrationAddressStreet,
                model.RegistrationAddressStreetKladrCode,
                model.RegistrationAddressHouse,
                model.RegistrationAddressBlock,
                model.RegistrationAddressBuilding,
                model.RegistrationAddressApartment,
                model.RegistrationAddressKladrCode,
                model.ResidenceAddressRegion,
                model.ResidenceAddressRegionKladrCode,
                model.ResidenceAddressCity,
                model.ResidenceAddressCityKladrCode,
                model.ResidenceAddressStreet,
                model.ResidenceAddressStreetKladrCode,
                model.ResidenceAddressHouse,
                model.ResidenceAddressBlock,
                model.ResidenceAddressBuilding,
                model.ResidenceAddressApartment,
                model.ResidenceAddressKladrCode
            };

            await using var connection = CreateConnection();
            return await connection.QueryFirstAsync<CreditApplicationCreatedEvent>(@"
                UPDATE credit_application
                SET
                   status = :status,
                   step = :step,
                   date = :date,
                   date_of_birth = :dateOfBirth,
                   place_of_birth = :placeOfBirth,
                   gender = :gender,
                   monthly_income = :monthlyIncome,
                   passport_series = :passportSeries,
                   passport_number = :passportNumber,
                   passport_issuer = :passportIssuer,
                   passport_issue_date = :passportIssueDate,
                   passport_department_code = :passportDepartmentCode,
                   registration_address_region = :registrationAddressRegion,
                   registration_address_region_kladr_code = :registrationAddressRegionKladrCode,
                   registration_address_city = :registrationAddressCity,
                   registration_address_city_kladr_code = :registrationAddressCityKladrCode,
                   registration_address_street = :registrationAddressStreet,
                   registration_address_street_kladr_code = :registrationAddressStreetKladrCode,
                   registration_address_house = :registrationAddressHouse,
                   registration_address_block = :registrationAddressBlock,
                   registration_address_building = :registrationAddressBuilding,
                   registration_address_apartment = :registrationAddressApartment,
                   registration_address_kladr_code = :registrationAddressKladrCode,
                   residence_address_region = :residenceAddressRegion,
                   residence_address_region_kladr_code = :residenceAddressRegionKladrCode,
                   residence_address_city = :residenceAddressCity,
                   residence_address_city_kladr_code = :residenceAddressCityKladrCode,
                   residence_address_street = :residenceAddressStreet,
                   residence_address_street_kladr_code = :residenceAddressStreetKladrCode,
                   residence_address_house = :residenceAddressHouse,
                   residence_address_block = :residenceAddressBlock,
                   residence_address_building = :residenceAddressBuilding,
                   residence_address_apartment = :residenceAddressApartment,
                   residence_address_kladr_code = :residenceAddressKladrCode
                WHERE
                    id = :creditApplicationId::uuid
                RETURNING 
                   id AS credit_application_id,
                   date,
                   profile_type,
                   partner_id,
                   last_name,
                   first_name,
                   middle_name,
                   phone_number,
                   credit_period,
                   credit_amount,
                   confirmation_code,
                   personal_data_process_approved,
                   personal_data_process_approve_date,
                   credit_bureau_process_approved,
                   credit_bureau_process_approve_date,
                   confirmation_code,
                   date_of_birth,
                   place_of_birth,
                   gender,
                   passport_series,
                   passport_number,
                   passport_issuer,
                   passport_issue_date,
                   passport_department_code,
                   registration_address_region,
                   registration_address_region_kladr_code,
                   registration_address_city,
                   registration_address_city_kladr_code,
                   registration_address_street,
                   registration_address_street_kladr_code,
                   registration_address_house,
                   registration_address_block,
                   registration_address_building,
                   registration_address_apartment,
                   registration_address_kladr_code,
                   residence_address_region,
                   residence_address_region_kladr_code,
                   residence_address_city,
                   residence_address_city_kladr_code,
                   residence_address_street,
                   residence_address_street_kladr_code,
                   residence_address_house,
                   residence_address_block,
                   residence_address_building,
                   residence_address_apartment,
                   residence_address_kladr_code,
                   monthly_income;", queryParams);
        }
    }
}