﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Models.Inner;
using Dapper;
using Microsoft.Extensions.Configuration;
using Yes.CreditApplication.Api.Contracts.Enums;

namespace CreditApplication.Api.Data.Public
{
    public interface IBaseCreditApplicationRepository
    {
        Task<CreditApplicationDraftModel> GetCreditApplicationDraft(Guid creditApplicationId);
        Task UpdateConfirmationCode(Guid creditApplicationId, string confirmationCode);
        Task ConfirmPhoneNumber(Guid creditApplicationId);
    }

    public class BaseCreditApplicationRepository : RepositoryBase, IBaseCreditApplicationRepository
    {
        public BaseCreditApplicationRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<CreditApplicationDraftModel> GetCreditApplicationDraft(Guid creditApplicationId)
        {
            await using var connection = CreateConnection();
            return await connection.QueryFirstOrDefaultAsync<CreditApplicationDraftModel>(@"
                SELECT
                    status,       
                    step,
                    confirmation_code,                    
                    phone_number,
                    created_date
                FROM credit_application
                WHERE
                    id = :creditApplicationId::uuid;", new {creditApplicationId});
        }

        public async Task UpdateConfirmationCode(Guid creditApplicationId, string confirmationCode)
        {
            var queryParams = new
            {
                creditApplicationId,
                confirmationCode,
                createdDate = DateTime.Now
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                UPDATE credit_application
                SET
                   confirmation_code = :confirmationCode,
                   created_date = :createdDate,
                   date = :createdDate
                WHERE
                    id = :creditApplicationId::uuid;", queryParams);
        }

        public async Task ConfirmPhoneNumber(Guid creditApplicationId)
        {
            var queryParams = new
            {
                creditApplicationId,
                step = CreditApplicationStep.ConfirmationCode,
                date = DateTime.Now,
                personalDataProcessApproved = true,
                creditBureauProcessApproved = true
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                UPDATE credit_application
                SET
                   step = :step,
                   date = :date,
                   personal_data_process_approved = :personalDataProcessApproved,
                   personal_data_process_approve_date = :date,
                   credit_bureau_process_approved = :creditBureauProcessApproved,
                   credit_bureau_process_approve_date = :date
                WHERE
                    id = :creditApplicationId::uuid;", queryParams);
        }
    }
}