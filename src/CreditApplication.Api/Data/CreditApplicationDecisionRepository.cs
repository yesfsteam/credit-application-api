﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;

namespace CreditApplication.Api.Data
{
    public interface ICreditApplicationDecisionRepository
    {
        Task CreateOrUpdateCreditApplicationDecision(Guid creditApplicationId, DateTime date, List<CreditApplicationStatusModel> creditApplications);
        Task<List<CreditOrganizationRequestStatus>> CreateOrUpdateCreditApplicationDecisionAndReturnAllStatuses(Guid creditApplicationId, DateTime date, List<CreditApplicationStatusModel> creditApplications);
        Task<List<CreditApplicationDecisionModel>> GetCreditApplicationDecisions(Guid creditApplicationId);
    }

    public class CreditApplicationDecisionRepository : RepositoryBase, ICreditApplicationDecisionRepository
    {
        public CreditApplicationDecisionRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task CreateOrUpdateCreditApplicationDecision(Guid creditApplicationId, DateTime date, List<CreditApplicationStatusModel> creditApplications)
        {
            await using var connection = CreateConnection();
            await connection.OpenAsync();
            await using var transaction = connection.BeginTransaction();
            try
            {
                foreach (var creditApplication in creditApplications)
                {
                    var queryParams = new
                    {
                        creditApplicationId,
                        creditApplication.CreditOrganizationId,
                        date,
                        creditApplication.Status,
                        creditApplication.CreditPeriod,
                        creditApplication.CreditAmount,
                        creditApplication.Details
                    };
                    await connection.ExecuteAsync(@"
                    INSERT INTO credit_application_decision (
                       created_date, 
                       updated_date, 
                       credit_application_id,
                       credit_organization_id,
                       status,
                       credit_period,                   
                       credit_amount,                   
                       details                   
                    ) VALUES (
                       :date,
                       :date,
                       :creditApplicationId::uuid,
                       :creditOrganizationId::uuid,
                       :status,
                       :creditPeriod,
                       :creditAmount,
                       :details
                    )
                    ON CONFLICT (credit_application_id, credit_organization_id) DO UPDATE 
                    SET
                       updated_date = :date,
                       status = :status,
                       credit_period = :creditPeriod,
                       credit_amount = :creditAmount,
                       details = :details;", queryParams, transaction);
                }
                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                throw;
            }
        }

        public async Task<List<CreditOrganizationRequestStatus>> CreateOrUpdateCreditApplicationDecisionAndReturnAllStatuses(Guid creditApplicationId, DateTime date, List<CreditApplicationStatusModel> creditApplications)
        {
            await using var connection = CreateConnection();
            await connection.OpenAsync();
            await using var transaction = connection.BeginTransaction();
            try
            {
                foreach (var creditApplication in creditApplications)
                {
                    var queryParams = new
                    {
                        creditApplicationId,
                        creditApplication.CreditOrganizationId,
                        date,
                        creditApplication.Status,
                        creditApplication.CreditPeriod,
                        creditApplication.CreditAmount,
                        creditApplication.Details
                    };
                    await connection.ExecuteAsync(@"
                    INSERT INTO credit_application_decision (
                       created_date, 
                       updated_date, 
                       credit_application_id,
                       credit_organization_id,
                       status,
                       credit_period,                   
                       credit_amount,                   
                       details                   
                    ) VALUES (
                       :date,
                       :date,
                       :creditApplicationId::uuid,
                       :creditOrganizationId::uuid,
                       :status,
                       :creditPeriod,
                       :creditAmount,
                       :details
                    )
                    ON CONFLICT (credit_application_id, credit_organization_id) DO UPDATE 
                    SET
                       updated_date = :date,
                       status = :status,
                       credit_period = :creditPeriod,
                       credit_amount = :creditAmount,
                       details = :details;", queryParams, transaction);
                }

                var decisions = await connection.QueryAsync<CreditOrganizationRequestStatus>($@"
                    SELECT
                        COALESCE(cad.status, {(int)CreditOrganizationRequestStatus.None})
                    FROM credit_organization co
                    CROSS JOIN (
                        SELECT
                            status
                        FROM
                            credit_application
                        WHERE
                            id = :creditApplicationId::uuid
                    ) ca
                    LEFT JOIN (
                        SELECT
                            updated_date,
                            credit_organization_id,
                            status,
                            credit_amount,
                            credit_period,
                            details
                        FROM credit_application_decision
                        WHERE
                            credit_application_id = :creditApplicationId::uuid
                    ) cad ON cad.credit_organization_id = co.id
                    WHERE
                        co.enabled = true;", new {creditApplicationId});
                await transaction.CommitAsync();
                return decisions.ToList();
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                throw;
            }
        }

        public async Task<List<CreditApplicationDecisionModel>> GetCreditApplicationDecisions(Guid creditApplicationId)
        {
            await using var connection = CreateConnection();
            return (await connection.QueryAsync<CreditApplicationDecisionModel>($@"
                SELECT
                    ca.status AS credit_application_status,
                    co.id AS credit_organization_id,
                    co.name AS credit_organization_name,
                    cad.updated_date,
                    COALESCE(cad.status, {(int)CreditOrganizationRequestStatus.None}) AS credit_organization_status,
                    cad.credit_amount,
                    cad.credit_period,
                    cad.details
                FROM credit_organization co
                CROSS JOIN (
                    SELECT
                        status
                    FROM
                        credit_application
                    WHERE
                        id = :creditApplicationId::uuid
                ) ca
                LEFT JOIN (
                    SELECT
                        updated_date,
                        credit_organization_id,
                        status,
                        credit_amount,
                        credit_period,
                        details
                    FROM credit_application_decision
                    WHERE
                        credit_application_id = :creditApplicationId::uuid
                ) cad ON cad.credit_organization_id = co.id
                WHERE
                    co.enabled = true;", new {creditApplicationId})).ToList();
        }
    }
}