﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;

namespace CreditApplication.Api.Data
{
    public interface ICreditApplicationRepository
    {
        //Admin
        Task<PagedResult<CreditApplicationListItemModel>> GetCreditApplications(CreditApplicationListFilterModel model);
        Task<List<CreditApplicationListItemExportModel>> GetCreditApplicationsForExport(CreditApplicationListFilterModel model);
        Task<CreditApplicationViewModel> GetCreditApplicationViewModel(Guid creditApplicationId);
        Task<CreditApplicationEditViewModel> GetCreditApplicationEditViewModel(Guid creditApplicationId);
        Task ChangeCreditApplicationStatus(Guid creditApplicationId, ChangeCreditApplicationStatusModel model);
        Task UpdateCreditApplication(Guid creditApplicationId, UpdateCreditApplicationModel model);
        
        //CreditOrganizations
        Task<ConfirmCreditApplicationRequest> GetCreditApplicationRequest(Guid creditApplicationId);

        //Scheduler
        Task<int> DeleteExpiredCreditApplications(DateTime date);
    }

    public class CreditApplicationRepository : RepositoryBase, ICreditApplicationRepository
    {
        public CreditApplicationRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<PagedResult<CreditApplicationListItemModel>> GetCreditApplications(CreditApplicationListFilterModel model)
        {
            var whereClause = getWhereClause(model);
            var queryParams = new
            {
                model.Take,
                model.Skip,
                model.DateFrom,
                model.DateTo,
                model.DateOfBirth,
                model.PhoneNumber,
                model.LastName,
                model.FirstName,
                model.MiddleName,
                profileTypes = model.ProfileTypes.Select(x => (int)x).ToList(),
                statuses = model.Statuses.Select(x => (int)x).ToList()
            };
            
            await using var connection = CreateConnection();
            var count = await connection.ExecuteScalarAsync<long>($@"
                SELECT
                    COUNT(*)
                FROM credit_application
                {whereClause}", queryParams);
            
            var creditApplications = (await connection.QueryAsync<CreditApplicationListItemModel>($@"
                SELECT
                    id AS credit_application_id,
                    date,
                    last_name,
                    first_name,
                    middle_name,
                    phone_number,
                    email,
                    credit_period,
                    credit_amount,
                    status
                FROM credit_application
                {whereClause}
                ORDER BY 
                    date DESC
                LIMIT :take 
                OFFSET :skip", queryParams)).ToList();
            
            return new PagedResult<CreditApplicationListItemModel>(creditApplications, count);
        }

        public async Task<List<CreditApplicationListItemExportModel>> GetCreditApplicationsForExport(CreditApplicationListFilterModel model)
        {
            var whereClause = getWhereClause(model);
            var queryParams = new
            {
                model.Take,
                model.Skip,
                model.DateFrom,
                model.DateTo,
                model.DateOfBirth,
                model.PhoneNumber,
                model.LastName,
                model.FirstName,
                model.MiddleName,
                profileTypes = model.ProfileTypes.Select(x => (int)x).ToList(),
                statuses = model.Statuses.Select(x => (int)x).ToList(),
            };
            
            await using var connection = CreateConnection();
            return (await connection.QueryAsync<CreditApplicationListItemExportModel>($@"
                select
                    ca.id AS credit_application_id,
                    ca.date,
                    ca.status,
                    p.name AS partner_name,
                    ca.profile_type,
                    ca.step,
                    ca.last_name,
                    ca.first_name,
                    ca.middle_name,
                    ca.phone_number,
                    ca.email,
                    ca.credit_period,
                    ca.credit_amount,
                    ca.personal_data_process_approve_date,
                    ca.credit_bureau_process_approve_date,
                    ca.date_of_birth,
                    ca.place_of_birth,
                    ca.gender,
                    ca.passport_series,
                    ca.passport_number,
                    ca.passport_issuer,
                    ca.passport_issue_date,
                    ca.passport_department_code,
                    ca.registration_address_region,
                    ca.registration_address_region_kladr_code,
                    ca.registration_address_city,
                    ca.registration_address_city_kladr_code,
                    ca.registration_address_street,
                    ca.registration_address_street_kladr_code,
                    ca.registration_address_house,
                    ca.registration_address_block,
                    ca.registration_address_building,
                    ca.registration_address_apartment,
                    ca.registration_address_kladr_code,
                    ca.residence_address_region,
                    ca.residence_address_region_kladr_code,
                    ca.residence_address_city,
                    ca.residence_address_city_kladr_code,
                    ca.residence_address_street,
                    ca.residence_address_street_kladr_code,
                    ca.residence_address_house,
                    ca.residence_address_block,
                    ca.residence_address_building,
                    ca.residence_address_apartment,
                    ca.residence_address_kladr_code,
                    ca.education,
                    ca.marital_status,
                    ca.dependents_count,
                    ca.confirmation_document,
                    ca.has_flat,
                    ca.has_house,
                    ca.has_area,
                    ca.has_car,
                    ca.additional_phone_number_type,
                    ca.additional_phone_number,
                    ca.activity,
                    ca.tin,
                    ca.monthly_income,
                    ca.employer_name,
                    ca.employer_address_region,
                    ca.employer_address_region_kladr_code,
                    ca.employer_address_city,
                    ca.employer_address_city_kladr_code,
                    ca.employer_address_street,
                    ca.employer_address_street_kladr_code,
                    ca.employer_address_house,
                    ca.employer_address_block,
                    ca.employer_address_building,
                    ca.employer_address_apartment,
                    ca.employer_address_kladr_code,
                    ca.employer_industry,
                    ca.employer_staff,
                    ca.employer_phone_number,
                    ca.employee_start_date,
                    ca.employee_position
                FROM credit_application ca
                INNER JOIN partner p ON p.id = ca.partner_id
                {whereClause}
                ORDER BY 
                    date DESC;", queryParams)).ToList();
        }

        private string getWhereClause(CreditApplicationListFilterModel model)
        {
            var queryBuilder = new StringBuilder();
            queryBuilder.AppendLine("WHERE 1 = 1");
            if (model.DateFrom.HasValue)
                queryBuilder.AppendLine("AND date >= :dateFrom");
            if (model.DateTo.HasValue)
            {
                model.DateTo = model.DateTo.Value.Date.AddDays(1);
                queryBuilder.AppendLine("AND date < :dateTo");
            }
            if (model.DateOfBirth.HasValue)
                queryBuilder.AppendLine("AND date_of_birth = :dateOfBirth");
            if (!string.IsNullOrWhiteSpace(model.PhoneNumber))
                queryBuilder.AppendLine("AND phone_number = :phoneNumber");
            if (!string.IsNullOrWhiteSpace(model.LastName))
                queryBuilder.AppendLine("AND last_name = :lastName");
            if (!string.IsNullOrWhiteSpace(model.FirstName))
                queryBuilder.AppendLine("AND first_name = :firstName");
            if (!string.IsNullOrWhiteSpace(model.MiddleName))
                queryBuilder.AppendLine("AND middle_name = :middleName");
            if (model.ProfileTypes?.Count > 0)
                queryBuilder.AppendLine("AND profile_type = ANY(:profileTypes)");
            else
                model.ProfileTypes = new List<ProfileType>();
            if (model.Statuses?.Count > 0)
                queryBuilder.AppendLine("AND status = ANY(:statuses)");
            else
                model.Statuses = new List<CreditApplicationStatus>();

            return queryBuilder.ToString();
        }
        
        public async Task<CreditApplicationViewModel> GetCreditApplicationViewModel(Guid creditApplicationId)
        {
            await using var connection = CreateConnection();
            return await connection.QueryFirstOrDefaultAsync<CreditApplicationViewModel>(@"
                SELECT
                    ca.id AS credit_application_id,
                    ca.last_name,
                    ca.first_name,
                    ca.middle_name,
                    ca.phone_number,
                    ca.email,
                    ca.credit_period,
                    ca.credit_amount,
                    ca.date,
                    p.name AS partner_name,
                    ca.status,
                    ca.step,
                    ca.profile_type
                FROM credit_application ca
                INNER JOIN partner p ON p.id = ca.partner_id 
                WHERE
                    ca.id = :creditApplicationId::uuid;", new {creditApplicationId});
        }

        public async Task<CreditApplicationEditViewModel> GetCreditApplicationEditViewModel(Guid creditApplicationId)
        {
            await using var connection = CreateConnection();
            return await connection.QueryFirstOrDefaultAsync<CreditApplicationEditViewModel>(@"
                SELECT
                    ca.id AS credit_application_id,
                    ca.status,
                    ca.last_name,
                    ca.first_name,
                    ca.middle_name,
                    ca.phone_number,
                    ca.email,
                    ca.credit_period,
                    ca.credit_amount,
                    ca.date_of_birth,
                    ca.place_of_birth,
                    ca.gender,
                    ca.passport_series,
                    ca.passport_number,
                    ca.passport_issuer,
                    ca.passport_issue_date,
                    ca.passport_department_code,
                    ca.registration_address_region,
                    ca.registration_address_region_kladr_code,
                    ca.registration_address_city,
                    ca.registration_address_city_kladr_code,
                    ca.registration_address_street,
                    ca.registration_address_street_kladr_code,
                    ca.registration_address_house,
                    ca.registration_address_block,
                    ca.registration_address_building,
                    ca.registration_address_apartment,
                    ca.registration_address_flat,
                    ca.registration_address_kladr_code,
                    ca.residence_address_region,
                    ca.residence_address_region_kladr_code,
                    ca.residence_address_city,
                    ca.residence_address_city_kladr_code,
                    ca.residence_address_street,
                    ca.residence_address_street_kladr_code,
                    ca.residence_address_house,
                    ca.residence_address_block,
                    ca.residence_address_building,
                    ca.residence_address_apartment,
                    ca.residence_address_flat,
                    ca.residence_address_kladr_code,
                    ca.education,
                    ca.marital_status,
                    ca.dependents_count,
                    ca.confirmation_document,
                    ca.has_flat,
                    ca.has_house,
                    ca.has_area,
                    ca.has_car,
                    ca.additional_phone_number_type,
                    ca.additional_phone_number,
                    ca.activity,
                    ca.tin,
                    ca.monthly_income,
                    ca.employer_name,
                    ca.employer_address_region,
                    ca.employer_address_region_kladr_code,
                    ca.employer_address_city,
                    ca.employer_address_city_kladr_code,
                    ca.employer_address_street,
                    ca.employer_address_street_kladr_code,
                    ca.employer_address_house,
                    ca.employer_address_block,
                    ca.employer_address_building,
                    ca.employer_address_apartment,
                    ca.employer_address_office,
                    ca.employer_address_kladr_code,
                    ca.employer_industry,
                    ca.employer_staff,
                    ca.employer_phone_number,
                    ca.employee_start_date,
                    ca.employee_position
                FROM credit_application ca
                WHERE
                    ca.id = :creditApplicationId::uuid;", new {creditApplicationId});
        }

        public async Task ChangeCreditApplicationStatus(Guid creditApplicationId, ChangeCreditApplicationStatusModel model)
        {
            var queryParams = new
            {
                creditApplicationId,
                status = model.Status
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                UPDATE credit_application
                SET
                   status = :status
                WHERE
                    id = :creditApplicationId::uuid;", queryParams);
        }

        public async Task UpdateCreditApplication(Guid creditApplicationId, UpdateCreditApplicationModel model)
        {
            var queryParams = new
            {
                creditApplicationId,
                model.LastName,
                model.FirstName,
                model.MiddleName,
                model.Email,
                model.CreditPeriod,
                model.CreditAmount,
                model.DateOfBirth,
                model.PlaceOfBirth,
                model.Gender,
                model.PassportSeries,
                model.PassportNumber,
                model.PassportIssueDate,
                model.PassportIssuer,
                model.PassportDepartmentCode,
                model.RegistrationAddressRegion,
                model.RegistrationAddressRegionKladrCode,
                model.RegistrationAddressCity,
                model.RegistrationAddressCityKladrCode,
                model.RegistrationAddressStreet,
                model.RegistrationAddressStreetKladrCode,
                model.RegistrationAddressHouse,
                model.RegistrationAddressBlock,
                model.RegistrationAddressBuilding,
                model.RegistrationAddressApartment,
                model.RegistrationAddressKladrCode,
                model.ResidenceAddressRegion,
                model.ResidenceAddressRegionKladrCode,
                model.ResidenceAddressCity,
                model.ResidenceAddressCityKladrCode,
                model.ResidenceAddressStreet,
                model.ResidenceAddressStreetKladrCode,
                model.ResidenceAddressHouse,
                model.ResidenceAddressBlock,
                model.ResidenceAddressBuilding,
                model.ResidenceAddressApartment,
                model.ResidenceAddressKladrCode,
                model.Education,
                model.MaritalStatus,
                model.DependentsCount,
                model.ConfirmationDocument,
                model.HasFlat,
                model.HasHouse,
                model.HasArea,
                model.HasCar,
                model.AdditionalPhoneNumberType,
                model.AdditionalPhoneNumber,
                model.Activity,
                model.Tin,
                model.MonthlyIncome,
                model.EmployerName,
                model.EmployerAddressRegion,
                model.EmployerAddressRegionKladrCode,
                model.EmployerAddressCity,
                model.EmployerAddressCityKladrCode,
                model.EmployerAddressStreet,
                model.EmployerAddressStreetKladrCode,
                model.EmployerAddressHouse,
                model.EmployerAddressBlock,
                model.EmployerAddressBuilding,
                model.EmployerAddressApartment,
                model.EmployerAddressKladrCode,
                model.EmployerIndustry,
                model.EmployerStaff,
                model.EmployerPhoneNumber,
                model.EmployeeStartDate,
                model.EmployeePosition
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                UPDATE credit_application
                SET
                    last_name = :lastName,
                    first_name = :firstName,
                    middle_name = :middleName,
                    email = :email,
                    credit_period = :creditPeriod,
                    credit_amount = :creditAmount,
                    date_of_birth = :dateOfBirth,
                    place_of_birth = :placeOfBirth,
                    gender = :gender,
                    passport_series = :passportSeries,
                    passport_number = :passportNumber,
                    passport_issuer = :passportIssuer,
                    passport_issue_date = :passportIssueDate,
                    passport_department_code = :passportDepartmentCode,
                    registration_address_region = :registrationAddressRegion,
                    registration_address_region_kladr_code = :registrationAddressRegionKladrCode,
                    registration_address_city = :registrationAddressCity,
                    registration_address_city_kladr_code = :registrationAddressCityKladrCode,
                    registration_address_street = :registrationAddressStreet,
                    registration_address_street_kladr_code = :registrationAddressStreetKladrCode,
                    registration_address_house = :registrationAddressHouse,
                    registration_address_block = :registrationAddressBlock,
                    registration_address_building = :registrationAddressBuilding,
                    registration_address_apartment = :registrationAddressApartment,
                    registration_address_kladr_code = :registrationAddressKladrCode,
                    residence_address_region = :residenceAddressRegion,
                    residence_address_region_kladr_code = :residenceAddressRegionKladrCode,
                    residence_address_city = :residenceAddressCity,
                    residence_address_city_kladr_code = :residenceAddressCityKladrCode,
                    residence_address_street = :residenceAddressStreet,
                    residence_address_street_kladr_code = :residenceAddressStreetKladrCode,
                    residence_address_house = :residenceAddressHouse,
                    residence_address_block = :residenceAddressBlock,
                    residence_address_building = :residenceAddressBuilding,
                    residence_address_apartment = :residenceAddressApartment,
                    residence_address_kladr_code = :residenceAddressKladrCode,
                    education = :education,
                    marital_status = :maritalStatus,
                    dependents_count = :dependentsCount,
                    confirmation_document = :confirmationDocument,
                    has_flat = :hasFlat,
                    has_house = :hasHouse,
                    has_area = :hasArea,
                    has_car = :hasCar,
                    additional_phone_number_type = :additionalPhoneNumberType,
                    additional_phone_number = :additionalPhoneNumber,
                    activity = :activity,
                    tin = :tin,
                    monthly_income = :monthlyIncome,
                    employer_name = :employerName,
                    employer_address_region = :employerAddressRegion,
                    employer_address_region_kladr_code = :employerAddressRegionKladrCode,
                    employer_address_city = :employerAddressCity,
                    employer_address_city_kladr_code = :employerAddressCityKladrCode,
                    employer_address_street = :employerAddressStreet,
                    employer_address_street_kladr_code = :employerAddressStreetKladrCode,
                    employer_address_house = :employerAddressHouse,
                    employer_address_block = :employerAddressBlock,
                    employer_address_building = :employerAddressBuilding,
                    employer_address_apartment = :employerAddressApartment,
                    employer_address_kladr_code = :employerAddressKladrCode,
                    employer_industry = :employerIndustry,
                    employer_staff = :employerStaff,
                    employer_phone_number = :employerPhoneNumber,
                    employee_start_date = :employeeStartDate,
                    employee_position = :employeePosition
                WHERE
                    id = :creditApplicationId::uuid;", queryParams);
        }

        public async Task<ConfirmCreditApplicationRequest> GetCreditApplicationRequest(Guid creditApplicationId)
        {
            await using var connection = CreateConnection();
            return await connection.QueryFirstOrDefaultAsync<ConfirmCreditApplicationRequest>(@"
                SELECT
                   profile_type,
                   partner_id,
                   last_name,
                   first_name,
                   middle_name,
                   phone_number,
                   email,
                   credit_period,
                   credit_amount,
                   confirmation_code,
                   personal_data_process_approved,
                   personal_data_process_approve_date,
                   credit_bureau_process_approved,
                   credit_bureau_process_approve_date,
                   date_of_birth,
                   place_of_birth,
                   gender,
                   passport_series,
                   passport_number,
                   passport_issuer,
                   passport_issue_date,
                   passport_department_code,
                   registration_address_region,
                   registration_address_region_kladr_code,
                   registration_address_city,
                   registration_address_city_kladr_code,
                   registration_address_street,
                   registration_address_street_kladr_code,
                   registration_address_house,
                   registration_address_block,
                   registration_address_building,
                   registration_address_apartment,
                   registration_address_kladr_code,
                   residence_address_region,
                   residence_address_region_kladr_code,
                   residence_address_city,
                   residence_address_city_kladr_code,
                   residence_address_street,
                   residence_address_street_kladr_code,
                   residence_address_house,
                   residence_address_block,
                   residence_address_building,
                   residence_address_apartment,
                   residence_address_kladr_code,
                   education,
                   marital_status,
                   dependents_count,
                   confirmation_document,
                   has_flat,
                   has_house,
                   has_area,
                   has_car,
                   additional_phone_number_type,
                   additional_phone_number,
                   activity,
                   tin,
                   monthly_income,
                   employer_name,
                   employer_address_region,
                   employer_address_region_kladr_code,
                   employer_address_city,
                   employer_address_city_kladr_code,
                   employer_address_street,
                   employer_address_street_kladr_code,
                   employer_address_house,
                   employer_address_block,
                   employer_address_building,
                   employer_address_apartment,
                   employer_address_kladr_code,
                   employer_industry,
                   employer_staff,
                   employer_phone_number,
                   employee_start_date,
                   employee_position
                FROM credit_application
                WHERE
                    id = :creditApplicationId::uuid;", new {creditApplicationId});
        }
        
        public async Task<int> DeleteExpiredCreditApplications(DateTime date)
        {
            await using var connection = CreateConnection();
            return await connection.ExecuteAsync(@"
                DELETE FROM credit_application
                WHERE
                    date < :date
                    AND personal_data_process_approved = false;", new {date});
        }
    }
}