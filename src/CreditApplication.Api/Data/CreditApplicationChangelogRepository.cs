﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditApplication.Api.Models.Commands;
using Dapper;
using Microsoft.Extensions.Configuration;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.Enums;

namespace CreditApplication.Api.Data
{
    public interface ICreditApplicationChangelogRepository
    {
        Task CreateCreditApplicationChangelogRecord(AddCreditApplicationChangelogRecordCommand command);
        Task<List<CreditApplicationChangeModel>> GetCreditApplicationChanges(Guid creditApplicationId);
    }

    public class CreditApplicationChangelogRepository : RepositoryBase, ICreditApplicationChangelogRepository
    {
        public CreditApplicationChangelogRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task CreateCreditApplicationChangelogRecord(AddCreditApplicationChangelogRecordCommand command)
        {
            var queryParams = new
            {
                command.CreditApplicationId,
                command.Date,
                command.Status,
                command.Comment
            };

            await using var connection = CreateConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_changelog (
                   credit_application_id, 
                   date, 
                   status,
                   comment
                ) VALUES (
                   :creditApplicationId::uuid,
                   :date,
                   :status,
                   :comment                   
                );", queryParams);
        }

        public async Task<List<CreditApplicationChangeModel>> GetCreditApplicationChanges(Guid creditApplicationId)
        {
            await using var connection = CreateConnection();
            return (await connection.QueryAsync<CreditApplicationChangeModel>($@"
                SELECT
                    date,
                    status,
                    comment                    
                FROM credit_application_changelog
                WHERE
                    credit_application_id = :creditApplicationId::uuid
                    AND status <> {(int)CreditApplicationStatus.InProgress}
                ORDER BY 
                    date DESC;", new {creditApplicationId})).ToList();
        }
    }
}