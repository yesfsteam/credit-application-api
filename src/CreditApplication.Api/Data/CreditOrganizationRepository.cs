﻿using System;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace CreditApplication.Api.Data
{
    public interface ICreditOrganizationRepository
    {
        Task<string> GetCreditOrganizationEndpoint(Guid creditOrganizationId);
    }

    public class CreditOrganizationRepository : RepositoryBase, ICreditOrganizationRepository
    {
        public CreditOrganizationRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<string> GetCreditOrganizationEndpoint(Guid creditOrganizationId)
        {
            await using var connection = CreateConnection();
            return await connection.QueryFirstOrDefaultAsync<string>(@"
                SELECT
                    endpoint
                FROM credit_organization
                WHERE
                    id = :creditOrganizationId::uuid;", new {creditOrganizationId});
        }
    }
}