﻿using Microsoft.Extensions.Configuration;
using Npgsql;

namespace CreditApplication.Api.Data
{
    public abstract class RepositoryBase
    {
        private readonly IConfiguration configuration;

        protected RepositoryBase(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        protected NpgsqlConnection CreateConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}