using System.IO;
using System.Net.Http;
using System.Reflection;
using CreditApplication.Api.Data;
using CreditApplication.Api.Domain;
using CreditApplication.Api.Extensions;
using CreditApplication.Api.Messaging;
using CreditApplication.Api.Models.Commands;
using CreditApplication.Api.Models.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using SD.Cqrs;
using SD.Cqrs.NetCore;
using SD.Logger.Serilog.NetCore;
using SD.Messaging.RabbitMQ.NetCore;
using Serilog;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Sms.Api.Contracts.Commands;

namespace CreditApplication.Api
{
    public class Startup
    {
	    private readonly string applicationName;
	    
	    public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            applicationName = Assembly.GetExecutingAssembly().GetName().Name;
        }

        private readonly IConfiguration configuration;
        
        public void ConfigureServices(IServiceCollection services)
        {
	        var messagingConfiguration = configuration.BindFromAppConfig<MessagingConfiguration>();
	        var applicationConfiguration = configuration.BindFromAppConfig<ApplicationConfiguration>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddCqrsEngine()
	            .WithSerilogLogger()
	            .WithRabbitMQMessagingEngine()
	            .WithBoundedContext(BoundedContext.Create(BoundedContexts.CREDIT_APPLICATION_API)
		            .PublishingEvents(typeof(CreditApplicationCreatedEvent))
		            .PublishingCommands(typeof(SendSmsCommand)).To(BoundedContexts.SMS_API)
		            .PublishingCommands(typeof(AddCreditApplicationChangelogRecordCommand)).To(BoundedContexts.CREDIT_APPLICATION_API).On(Routes.Changes)
		            .ListeningCommands(typeof(CreditApplicationDecisionsCommand)).On(Routes.Decisions)
		            .ListeningCommands(typeof(AddCreditApplicationChangelogRecordCommand)).On(Routes.Changes)
		            .Build())
	            .AddCommandHandler<CreditOrganizationsCommandHandler>(Routes.Decisions, messagingConfiguration.ChannelsCount)
	            .AddCommandHandler<CreditOrganizationsCommandHandler>(Routes.Changes, messagingConfiguration.ChannelsCount);
            
            services.AddTransient<Data.Public.FullProfile.ICreditApplicationRepository, Data.Public.FullProfile.CreditApplicationRepository>();
            services.AddTransient<Data.Public.MediumProfile.ICreditApplicationRepository, Data.Public.MediumProfile.CreditApplicationRepository>();
            services.AddTransient<Data.Public.ShortProfile.ICreditApplicationRepository, Data.Public.ShortProfile.CreditApplicationRepository>();
            services.AddTransient<ICreditApplicationRepository, CreditApplicationRepository>();
            services.AddTransient<ICreditApplicationDecisionRepository, CreditApplicationDecisionRepository>();
            services.AddTransient<ICreditApplicationChangelogRepository, CreditApplicationChangelogRepository>();
            services.AddTransient<ICreditOrganizationRepository, CreditOrganizationRepository>();
            
            services.AddTransient<Domain.Public.FullProfile.ICreditApplicationManager, Domain.Public.FullProfile.CreditApplicationManager>();
            services.AddTransient<Domain.Public.MediumProfile.ICreditApplicationManager, Domain.Public.MediumProfile.CreditApplicationManager>();
            services.AddTransient<Domain.Public.ShortProfile.ICreditApplicationManager, Domain.Public.ShortProfile.CreditApplicationManager>();
            services.AddTransient<ICreditApplicationManager, CreditApplicationManager>();
            services.AddTransient<ICreditApplicationDecisionManager, CreditApplicationDecisionManager>();
            services.AddTransient<ICreditApplicationChangelogManager, CreditApplicationChangelogManager>();
            services.AddTransient<IConfirmationCodeGenerator, ConfirmationCodeGenerator>();
            
            services.AddSingleton(applicationConfiguration);
            services.AddSingleton<ICreditOrganizationClient, CreditOrganizationClient>();
            services.AddSingleton(new HttpClient{ Timeout = applicationConfiguration.CreditOrganizationTimeout });
            
            if (configuration.GetValue<bool>("EnableSwagger"))
	            services.AddSwaggerGen(c =>
	            {
		            c.SwaggerDoc("v1", new OpenApiInfo { Title = applicationName, Version = "v1" });
		            c.SwaggerDoc("v2", new OpenApiInfo { Title = applicationName, Version = "v2" });
		            c.SwaggerDoc("v3", new OpenApiInfo { Title = applicationName, Version = "v3" });
		            c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, $"{applicationName}.xml"));

		            c.DocInclusionPredicate((version, apiDescription) =>
		            {
			            if (apiDescription.RelativePath.Contains("/" + version + "/"))
			            {
				            return true;
			            }
			            return false;
		            });
	            });
           
			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

            DapperInitializer.ConfigureDapper();
		}

        public void Configure(IApplicationBuilder app, IHostApplicationLifetime applicationLifetime, CqrsEngine cqrsEngine)
        {
	        applicationLifetime.ApplicationStopping.Register(()=>OnApplicationStopping(cqrsEngine));

	        app.UseRouting();
	        app.UseEndpoints(endpoints => endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"));

	        if (configuration.GetValue<bool>("EnableSwagger"))
	        {
		        app.UseSwagger();
		        app.UseSwaggerUI(c =>
		        {
			        c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{applicationName} v1 FullProfile");
			        c.SwaggerEndpoint("/swagger/v2/swagger.json", $"{applicationName} v2 MediumProfile");
			        c.SwaggerEndpoint("/swagger/v3/swagger.json", $"{applicationName} v3 ShortProfile");
			        c.RoutePrefix = string.Empty;
		        });
	        }
            
	        cqrsEngine.Start();
            
	        Log.Logger.Information($"{applicationName} has been started");
        }

        private void OnApplicationStopping(CqrsEngine cqrsEngine)
        {
	        cqrsEngine.Stop();
	        Log.Logger.Information($"{applicationName} has been stopped");
        }
	}
}