﻿using CreditApplication.Api.Models.Enums;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Extensions
{
    public static class ErrorCodeExtensions
    {
        public static ErrorModel ToErrorModel(this ErrorCode errorCode)
        {
            return new ErrorModel
            {
                ErrorCode = (int) errorCode,
                Message = errorCode.GetDescription()
            };
        }
    }
}