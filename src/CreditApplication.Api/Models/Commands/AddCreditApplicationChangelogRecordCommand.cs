﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Models.Commands
{
    public class AddCreditApplicationChangelogRecordCommand : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата изменения заявки на кредит
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Статус заявки на кредит
        /// </summary>
        public CreditApplicationStatus Status { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }
        
    }
}