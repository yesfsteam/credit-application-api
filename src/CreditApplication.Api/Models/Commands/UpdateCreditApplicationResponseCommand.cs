﻿using System;
using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Models.Commands
{
    public class UpdateCreditApplicationResponseCommand : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Идентификатор кредитной организации
        /// </summary>
        public Guid CreditOrganizationId { get; set; }
        
        /// <summary>
        /// Признак успешного одобрения кредита. Если значение = null, значит ответ от кредитной организации еще не получен
        /// </summary>
        public bool? Approved { get; set; }
        
        /// <summary>
        /// Признак успешного одобрения кредита
        /// </summary>
        public DateTime UpdatedDate { get; set; }
    }
}