﻿using System;
using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Models.Commands
{
    public class CreateCreditApplicationResponseCommand : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Идентификатор кредитной организации
        /// </summary>
        public Guid CreditOrganizationId { get; set; }
    }
}