﻿using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Models.Admin
{
    public class CreditApplicationListFilterModel : JsonModel
    {
        /// <summary>
        /// Пропустить Skip элементов
        /// </summary>
        public int Skip { get; set; }

        /// <summary>
        /// Количество элементов для отображения
        /// </summary>
        public int Take { get; set; }
    }
}