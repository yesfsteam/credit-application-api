﻿namespace CreditApplication.Api.Models.Configuration
{
    public class MessagingConfiguration
    {
        public int ChannelsCount { get; set; }
    }
}