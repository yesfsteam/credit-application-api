﻿using System;

namespace CreditApplication.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public TimeSpan ConfirmationCodeLifetime { get; set; }
        public TimeSpan ResendSmsPeriod { get; set; }
        public TimeSpan CreditOrganizationTimeout { get; set; }
        public string ConfirmationCodeSmsTemplate { get; set; }
        public string OffHoursSmsTemplate { get; set; }
        public int WorkTimeHoursFrom { get; set; }
        public int WorkTimeHoursTo { get; set; }
        public TimeSpan NotConfirmedCreditApplicationsLifetime { get; set; }
    }
}