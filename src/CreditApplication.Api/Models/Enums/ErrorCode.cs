﻿﻿using System.ComponentModel;

 namespace CreditApplication.Api.Models.Enums
{
    public enum ErrorCode
    {
        [Description("Невалидные данные")]
        IndalidData = 0,
        [Description("Время жизни кода подтверждения истекло")]
        ConfirmationCodeExpired = 1,
        [Description("С момента отправки последнего кода подтверждения прошло недостаточно времени")]
        ResendSmsPeriodNotPassed = 2,
        [Description("Неверный код подтверждения")]
        ConfirmationCodeDoesNotMatch = 3,
        [Description("Статус заявки не допускает изменений")]
        CreditApplicationInvalidStatus = 4,
        [Description("Номер телефона не подтвержден")]
        PhoneNumberNotConfirmed = 5,
        [Description("Не предоставлены дополнительные данные заявителя (шаг 3)")]
        AdditionalInformationNotSpecified = 5
    }
}