﻿using System;
using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Models.Inner
{
    public class CreditApplicationCreatedModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата создания заявки на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}