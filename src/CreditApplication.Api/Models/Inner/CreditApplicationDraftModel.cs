﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace CreditApplication.Api.Models.Inner
{
    public class CreditApplicationDraftModel : JsonModel
    {
        /// <summary>
        /// Статус заявки на кредит
        /// </summary>
        public CreditApplicationStatus Status { get; set; }
        
        /// <summary>
        /// Шаг ввода информации клиентом
        /// </summary>
        public CreditApplicationStep Step { get; set; }
        
        /// <summary>
        /// Код подтверждения
        /// </summary>
        public string ConfirmationCode { get; set; }
        
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Дата создания заявки на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}