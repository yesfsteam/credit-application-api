﻿using System;
using System.Threading.Tasks;
using CreditApplication.Api.Domain;
using CreditApplication.Api.Models.Commands;
using SD.Cqrs;
using SD.Messaging.Models;
using Yes.CreditApplication.Api.Contracts.Commands;

namespace CreditApplication.Api.Messaging
{
    public class CreditOrganizationsCommandHandler : CommandHandler
    {
        private readonly ICreditApplicationDecisionManager manager;
        private readonly ICreditApplicationChangelogManager changelogManager;

        public CreditOrganizationsCommandHandler(ICreditApplicationDecisionManager manager, 
            ICreditApplicationChangelogManager changelogManager)
        {
            this.manager = manager;
            this.changelogManager = changelogManager;
        }

        public async Task<ProcessingResult> Handle(CreditApplicationDecisionsCommand command)
            => await manager.ProcessCreditApplicationDecision(command);
        
        public async Task<ProcessingResult> Handle(AddCreditApplicationChangelogRecordCommand command)
            => await changelogManager.ProcessCreditApplicationChange(command);
    }
}